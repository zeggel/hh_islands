import unittest
from island.Island import Island

class TestIsland(unittest.TestCase):
    def test_find_coast_and_land_indexes(self):
        params = (
            (1, 1, [0], []),
            (2, 2, [0, 1, 2, 3], []),
            (2, 3, [0, 1, 2, 3, 4, 5], []),
            (3, 3, [0, 1, 2, 3, 5, 6, 7, 8], [4]),
        )
        for width, height, coast_indexes, land_indexes in params:
            island = Island(width, height, [0]*(width*height))
            self.assertEqual(coast_indexes, sorted(island.coast_indexes), 'For %i x %i' % (width, height))
            self.assertEqual(land_indexes, sorted(island.land_indexes), 'For %i x %i' % (width, height))

    def test_get_siblings(self):
        island = Island(3, 3, [0]*9)
        params = (
            (0, [1, 3]),
            (1, [0, 2, 4]),
            (2, [1, 5]),
            (3, [0, 4, 6]),
            (4, [1, 3, 5, 7]),
            (5, [2, 4, 8]),
            (6, [3, 7]),
            (7, [4, 6, 8]),
            (8, [5, 7]),
        )
        for index, siblings in params:
            actual = sorted(island.get_siblings(index))
            self.assertEqual(siblings, actual, 'For %i - %s' % (index, str(actual)))
            
    def test_search_path(self):    
        island = Island(4, 4, [5, 3, 4, 5, 6, 2, 1, 4, 3, 1, 1, 4, 8, 5, 4, 3])
        to_fill = []
        border = []
        island.search_path(1, 6, to_fill, border)
        self.assertEqual([6, 9, 10], sorted(to_fill), 'Actual to fill: ' + str(to_fill))
        self.assertEqual([2, 5, 7, 8, 11, 13, 14], sorted(border), 'Actual border: ' + str(border))
        
    def test_fill_by_index(self):
        island = Island(4, 4, [5, 3, 4, 5, 6, 2, 1, 4, 3, 1, 1, 4, 8, 5, 4, 3])
        params = (
            (0, 0),
            (5, 0),
            (6, 7),
        )
        for index, volume in params:
            actual = island.fill_by_index(index)
            #print island.levels
            self.assertEqual(volume, actual, 'Actual volume for %i is %i' % (index, actual))

    def test_fill(self):
        #from numpy.random.mtrand import randint
        params = (
            (2, 2, [0]*4, 0),
            (3, 3, [3, 3, 3, 3, 1, 3, 3, 3, 3], 2),
            (4, 4, [5, 3, 4, 5, 6, 2, 1, 4, 3, 1, 1, 4, 8, 5, 4, 3], 7),
            #(50, 50, randint(1, 1000, size=2500), 0)
        )
        for width, height, levels, volume in params:
            island = Island(width, height, levels)
            actual = island.fill()
            self.assertEqual(volume, actual, 'For %s actual is %i' % (str(levels), actual))

if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()