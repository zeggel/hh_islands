# encoding: utf-8
'''
Класс используется для заполнения острова водой и вычисления необходимого объема воды
'''
class Island(object):
    '''
    Конструктор
    @param width: ширина острова
    @param height: высота острова
    @param levels: уровни холмов на острове в виде списка
    '''
    def __init__(self, width, height, levels):
        self.width = width
        self.height = height
        self.area = width * height
        self.levels = levels
        self.find_coast_and_land_indexes()
        
    '''
    Вычисляет индексы холмов, которые находятся на берегах и в глубине острова
    '''
    def find_coast_and_land_indexes(self):
        self.land_indexes = []
        if self.width < 3 or self.height < 3:
            self.coast_indexes = range(self.area)
            return
        self.coast_indexes = range(self.width)
        self.coast_indexes += range(self.area - self.width, self.area)
        for i in range(self.width, self.area - self.width):
            if i % self.width == 0 or (i + 1) % self.width == 0:
                self.coast_indexes.append(i)
            else:
                self.land_indexes.append(i)
        
    '''
    Возвращает индексы соседних холмов для указанного холма
    @param index: индекс холма для которого необходимо получить соседей
    '''
    def get_siblings(self, index):
        if index == 0:
            return [1, self.width]
        if index == self.width - 1:
            return [index - 1, index + self.width]
        if index == self.area - self.width:
            return [index - self.width, index + 1]
        if index == self.area - 1:
            return [index - self.width, index - 1]
        
        if index < self.width:
            return [index - 1, index + self.width, index + 1]
        if index >= self.area - self.width:
            return [index - 1, index - self.width, index + 1]
        
        if index % self.width == 0:
            return [index - self.width, index + 1, index + self.width]
        if (index + 1) % self.width == 0:
            return [index - self.width, index - 1, index + self.width]
        
        return [index - self.width, index - 1, index + 1, index + self.width]
    
    '''
    Заполняет холмы, начиная с самого низкого и возвращает используемый объем воды
    '''
    def fill(self):
        indexes_to_fill = sorted(self.land_indexes, lambda x, y: cmp(self.levels[x], self.levels[y]))
        volume = 0
        while indexes_to_fill:
            next_ = indexes_to_fill.pop(0)
            volume += self.fill_by_index(next_)
            
        return volume
            
    '''
    По возможности заполняет холм и всех его соседей с такой же высотой и возвращает объем используемой воды
    @param index: индекс холма для заполнения
    '''
    def fill_by_index(self, index):
        if index in self.coast_indexes:
            return 0
        
        volume = 0
        level = self.levels[index]
        
        while True:
            to_fill = []
            border = []
            
            self.search_path(level, index, to_fill, border)
            next_level = self.get_min_level(border)
            if (next_level > level):
                for to_fill_index in to_fill:
                    self.levels[to_fill_index] = next_level    
                volume += (next_level - level) * len(to_fill)
            else:
                break
            level = next_level
        
        return volume
    
    '''
    Вычисляет минимальный уровень из указанных холмов
    @param indexes: Списох индексов холмов
    '''
    def get_min_level(self, indexes):
        return min([self.levels[i] for i in indexes])

    '''
    Ищет путь для заполнения, возвращает индексы для заполнения и индексы границы
    @param initial_level: Уровент начального холма
    @param index: индекс начального холма
    @param to_fill: Индексы холмов для заполнения
    @param border: Индексы холмов, которые являются границей холмов для заполнения
    '''
    def search_path(self, initial_level, index, to_fill, border):
        queue = [index]
        visited = [index]
        to_fill.append(index)
        while queue:
            next_ = queue.pop()
            siblings = self.get_siblings(next_)
            for sibling in siblings:
                if sibling in visited:
                    continue
                visited.append(sibling)
                if sibling not in self.coast_indexes and self.levels[sibling] == initial_level:
                    to_fill.append(sibling)
                    queue.append(sibling)
                else:
                    border.append(sibling)
                    
        #border = [i for i in visited if i not in to_fill]
    
    '''
    Проверяет что уровень холмов больше указаного уровня
    @param level: Проверяемый уровень
    @param border: Индексы проверяемых холмов
    '''
    def has_only_higher_coast(self, level, border):
        if not border:
            return True
        
        for index in border:
            if index in self.coast_indexes and self.levels[index] <= level:
                return False
            
        return True
        
        