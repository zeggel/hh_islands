import sys
from island.Island import Island

def parse_dimension(str_):
    dimension = str_.strip().split(' ')
    
    return (int(dimension[0]), int(dimension[1]))

def parse_ints(count, str_):
    ints = str_.strip().split(' ')
    if len(ints) != count:
        raise ValueError
    
    return map(int, ints)

island_count = int(sys.stdin.readline())
islands = []

for i in range(island_count):
    height, width = parse_dimension(sys.stdin.readline())

    levels = []
    for j in range(height):
        line = sys.stdin.readline()
        levels += parse_ints(width, line)
        
    islands.append(Island(width, height, levels))
    
for island in islands:
    print island.fill()
    